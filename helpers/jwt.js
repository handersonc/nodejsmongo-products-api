

const expressJwt = require('express-jwt');
const config = require('../config');
const User = require('../models/user.model');

module.exports = jwt;

function jwt() {
    const secret = config.secret;
    return expressJwt({ secret, isRevoked }).unless({
        path: [
            '/api/v1/signin',
            '/api/v1/users'
        ]
    });
}

async function isRevoked(req, payload, done) {
    console.log('dddddddddddddddd', payload);
    const user = await User.findById(payload.userId);

    // revoke token if user no longer exists
    if (!user) {
        return done(null, true);
    }

    done();
};