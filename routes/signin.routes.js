module.exports = (app) => {
    const auth = require('../controllers/auth.controller.js');

    // authenticate a user
    app.post('/api/v1/signin', auth.signin);

}