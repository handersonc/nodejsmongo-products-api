var guard = require('express-jwt-permissions')();

module.exports = (app) => {
    const products = require('../controllers/product.controller.js');

    // Create a new Product
    app.post('/api/v1/products', guard.check(['product:create']), products.create);

    // Retrieve all Products
    app.get('/api/v1/products', guard.check(['product:list']), products.findAll);

    // Retrieve a single Product with productId
    app.get('/api/v1/products/:productId', guard.check(['product:read']), products.findOne);

    // Update a Note with productId
    app.put('/api/v1/products/:productId', guard.check(['product:edit']), products.update);

    // Delete a Note with productId
    app.delete('/api/v1/products/:productId', guard.check(['product:delete']), products.delete);

}