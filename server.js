// get dependencies
const express = require('express');
const bodyParser = require('body-parser');
const errorHandler = require('./helpers/errors');
const jwt = require('./helpers/jwt');

const app = express();

// parse requests
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//Enable CORS for all HTTP methods
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});

app.use(jwt());
app.use(errorHandler);

// Configuring the database
const config = require('./config.js');
const mongoose = require('mongoose');

// adding routes
require('./routes/product.routes.js')(app);
require('./routes/user.routes.js')(app);
require('./routes/signin.routes.js')(app);

// Connecting to the database
mongoose.connect(config.url, {
    useNewUrlParser: true
}).then(() => {
    console.log("Successfully connected to the database");    
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});

mongoose.Promise = global.Promise;

// default route
app.get('/', (req, res) => {
    res.json({"message": "Welcome to Product app"});
});

// listen on port 3000
app.listen(config.serverport, () => {
    console.log("Server is listening on port 3000");
});