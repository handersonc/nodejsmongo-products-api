
var jwt = require('jsonwebtoken')
const User = require('../models/user.model');
const config = require('../config');

exports.signin = (req, res) => {
    var email = req.body.email
    var password = req.body.password

    User.findOne({email: email, password: password}).then(user => {
        console.log('****************', user);
        if(!user){
            res.status(401).send({
            error: 'usuario o contraseña inválidos'
            })
            return
        }

        let permissions = {
            'admin': [
                'product:list', 'product:read', 'product:edit', 'product:delete', 'product:create',
                'user:list', 'user:read', 'user:edit', 'user:delete', 'user:create'
            ],
            'user': ['product:list']
        }
    
        var tokenData = {
            email: email,
            role: user.role,
            userId: user.id,
            permissions: permissions[user.role]
        }

        console.log('payload', tokenData)
    
        var token = jwt.sign(tokenData, config.secret, {
            expiresIn: 60 * 60 * 24 // expires in 24 hours
        })
    
        res.send({
            token
        });
    }).catch(err => {
        return res.status(500).send({
            message: "Something wrong signin the user" + req.email
        });
    });

};